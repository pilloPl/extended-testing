package io.pillopl.leave;

public enum Result {

    Approved, Denied, Manual
}
