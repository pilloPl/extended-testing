package io.pillopl.bigballofmud;


import io.pillopl.bigballofmud.controllers.BookController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {BigBallOfMud.class})
public class HoldingAndCollectingIntegrationTest {

    @Autowired
    IntegrationFixtures fixtures;

    @Autowired
    BookController bookController;

    @Test
    public void researcherPatronCanHoldRestrictedBooks() {

    }

    @Test
    public void patronCanHoldABook() {

    }

    @Test
    public void canCollectABook() {

    }


}


