package io.pillopl.bigballofmud;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class HoldingAndCollectingUnitTest {

    UnitFixtures fixtures;

    @Test
    public void regularPatronCannotHoldRestrictedBooks() {

    }

    @Test
    public void researcherPatronCanHoldRestrictedBooks() {

    }

    @Test
    public void canCollectABook() {

    }

}


